const Log = require('log');

const config = require('../config');
const utils = require('../utils/utils');

const log = new Log(config.LOG_LEVEL);

module.exports.handle = (message) => {
  if (message.from
      && message.chat
      && config.ADMIN_USER_IDS.includes(message.from.id)
      && config.GROUP_ID == message.chat.id) {
    log.debug(`Forwarding message:\n${utils.formatJson(message)}`);

    return {
      method: config.FW_MSG_METHOD,
      chat_id: config.ANNOUNCEMENTS_GROUP_ID,
      from_chat_id: message.chat.id,
      message_id: message.message_id
    };
  } else {
    log.info(`Ignoring message:\n${utils.formatJson(message)}`);
    return config.BASIC_RESPONSE;
  }
};
