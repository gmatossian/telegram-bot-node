const Log = require('log');

const config = require('../config');
const utils = require('../utils/utils');

const log = new Log(config.LOG_LEVEL);

module.exports.handle = (update) => {
  log.info(`Ignoring update:\n${utils.formatJson(update)}`);
  return config.BASIC_RESPONSE;
};
