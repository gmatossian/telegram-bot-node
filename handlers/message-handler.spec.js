const expect = require('expect');

const config = require('../config');
const messageHandler = require('./message-handler');

describe('handle', () => {
  it('forwards message if sent by an admin', () => {
    const msg = {
      chat: { id: config.GROUP_ID },
      from: { id: config.ADMIN_USER_IDS[0] },
      message_id: 'foo'
    };
    expect(messageHandler.handle(msg)).toEqual({
      method: config.FW_MSG_METHOD,
      chat_id: config.ANNOUNCEMENTS_GROUP_ID,
      from_chat_id: config.GROUP_ID,
      message_id: 'foo'
    });
  });
  
  it('returns basic response if message sent to a different group', () => {
    const msg = {
      chat: { id: 'another group id' },
      from: { id: config.ADMIN_USER_IDS[0] },
      message_id: 'foo'
    };
    expect(messageHandler.handle(msg)).toEqual({});
  });
  
  it('returns basic response if message not sent by an admin', () => {
    const msg = {
      chat: { id: config.GROUP_ID },
      from: { id: 'not an admin id' },
      message_id: 'foo'
    };
    expect(messageHandler.handle(msg)).toEqual({});
  });
});
