const expect = require('expect');

const config = require('../config');
const unsupportedUpdateHandler = require('./unsupported-update-handler');

describe('handle', () => {
  it('returns basic response', () => {
    expect(unsupportedUpdateHandler.handle({})).toEqual({});
  });
});
