const utils = require('./utils');
const expect = require('expect');
const spyOn = expect.spyOn;

describe('formatJson', () => {
  it('converts JSON to string', () => {
    spyOn(utils, 'formatJson').andCallThrough();
    expect(utils.formatJson({})).toBe(`{}`);
    expect(utils.formatJson).toHaveBeenCalled();
  });
  
  it('ignores properties with null values', () => {
    expect(utils.formatJson({ foo: null })).toBe(`{}`);
  });
  
  it('ignores properties with zero value', () => {
    expect(utils.formatJson({ foo: 0 })).toBe(`{}`);
  });
  
  it('uses 2 spaces for indentation', () => {
    expect(utils.formatJson({ foo: { bar: 'baz' } })).toBe(
`{
  "foo": {
    "bar": "baz"
  }
}`
    );
  });
});
