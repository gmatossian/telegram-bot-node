module.exports.formatJson = (str) => JSON.stringify(
  str,
  (key, value) => ((value === null || value === 0) ? undefined : value),
  2
);
