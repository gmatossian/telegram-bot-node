# README #

Basic Telegram Bot to FW admin messages in a group onto another group.

### What is this repository for? ###

Basic Telegram Bot to forward messages in a group sent by the admin to a different group (which will only contain then admin messages and no user messages).
More info: https://gmatossian.wordpress.com/2017/12/27/telegram-bot/.

### How do I get set up? ###

Running tests (1 off):
$ npm test
Note: requires mocha.

Running the app:
$ npm start

Running tests and watching files (keeps running):
$ npm run test-watch
Note: requires nodemon.