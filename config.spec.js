const expect = require('expect');

const config = require('./config');

it('defines configuration properties', () => {
  expect(config).toEqual({
    ADMIN_USER_IDS: [429746285, 455414240],
    GROUP_ID: -1001210575854,
    ANNOUNCEMENTS_GROUP_ID: "-253315974",
    FW_MSG_METHOD: "forwardMessage",
    BASIC_RESPONSE: {},
    LOG_LEVEL: 'info'
  });
});
