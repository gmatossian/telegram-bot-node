const express = require('express');
const bodyParser = require('body-parser');
const Log = require('log');

const config = require('./config');
const utils = require('./utils/utils');
let messageHandler = require('./handlers/message-handler'); // Using let so that rewire works
let unsupportedUpdateHandler = require('./handlers/unsupported-update-handler'); // Same here

const log = new Log(config.LOG_LEVEL);
let port = process.env.PORT || 8080;
const app = express(); // See https://stackoverflow.com/a/33261362/2670881

app.use(bodyParser.json());

app.post('/bot', (req, resp) => {
  const update = req.body;

  log.info(`Update received:\n${utils.formatJson(req.body)}`);
		
  if (update.message != null) {
    log.debug("Handling new message...");
    resp.send(messageHandler.handle(update.message));
  } else if (update.edited_message != null) {
    log.debug("Handling edited message...");
    resp.send(messageHandler.handle(update.edited_message));
  } else {
    log.debug("Handling unsupported update type...");
    resp.send(unsupportedUpdateHandler.handle(update));
  }
});

app.listen(port);

module.exports.app = app;
