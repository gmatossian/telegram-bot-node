const rewire = require('rewire');
const request = require('supertest');
const expect = require('expect');
const spyOn = expect.spyOn;
const createSpy = expect.createSpy;

let server = rewire('./server');

const messageHandler = server.__get__('messageHandler');
const unsupportedUpdateHandler = server.__get__('unsupportedUpdateHandler');

const app = server.app;

describe('POST /bot', function() {
  const dummyMessage = {
    from: 'foo',
    chat: 'bar',
    from: { id: 123 },
    chat: { id: '456' }
  };

  beforeEach(() => {
    spyOn(messageHandler, 'handle').andReturn({ "handler": "message handler" });
    spyOn(unsupportedUpdateHandler, 'handle').andReturn({ "handler": "unsupported update handler" });
  });

  it('generates a response using message handler if body has a message', function(done) {
    request(app)
      .post('/bot')
      .send({ message: dummyMessage })
      .expect(200)
      .expect({ "handler": "message handler" })
      .end(function(err, res) {
        if (err) return done(err);
        expect(messageHandler.handle).toHaveBeenCalledWith(dummyMessage);
        done();
      });
  });

  it('generates a response using message handler if body has an edited message', function(done) {
    request(app)
      .post('/bot')
      .send({ edited_message: dummyMessage })
      .expect(200)
      .expect({ "handler": "message handler" })
      .end(function(err, res) {
        if (err) return done(err);
        expect(messageHandler.handle).toHaveBeenCalledWith(dummyMessage);
        done();
      });
  });

  it('generates a response using unsupported update handler if body has a channel post', function(done) {
    request(app)
      .post('/bot')
      .send({ "channel_post": {} })
      .expect(200)
      .expect({ "handler": "unsupported update handler" })
      .end(function(err, res) {
        if (err) return done(err);
        expect(unsupportedUpdateHandler.handle).toHaveBeenCalledWith({ "channel_post": {} });
        done();
      });
  });

  it('generates a response using unsupported update handler if body has an edited channel post', function(done) {
    request(app)
      .post('/bot')
      .send({ "edited_channel_post": {} })
      .expect(200)
      .expect({ "handler": "unsupported update handler" })
      .end(function(err, res) {
        if (err) return done(err);
        expect(unsupportedUpdateHandler.handle).toHaveBeenCalledWith({ "edited_channel_post": {} });
        done();
      });
  });

  it('generates a response using unsupported update handler if body has an inline query', function(done) {
    request(app)
      .post('/bot')
      .send({ "inline_query": {} })
      .expect(200)
      .expect({ "handler": "unsupported update handler" })
      .end(function(err, res) {
        if (err) return done(err);
        expect(unsupportedUpdateHandler.handle).toHaveBeenCalledWith({ "inline_query": {} });
        done();
      });
  });

  it('generates a response using unsupported update handler if body has a chosen inline result', function(done) {
    request(app)
      .post('/bot')
      .send({ "chosen_inline_result": {} })
      .expect(200)
      .expect({ "handler": "unsupported update handler" })
      .end(function(err, res) {
        if (err) return done(err);
        expect(unsupportedUpdateHandler.handle).toHaveBeenCalledWith({ "chosen_inline_result": {} });
        done();
      });
  });

  it('generates a response using unsupported update handler if body has a callback query', function(done) {
    request(app)
      .post('/bot')
      .send({ "callback_query": {} })
      .expect(200)
      .expect({ "handler": "unsupported update handler" })
      .end(function(err, res) {
        if (err) return done(err);
        expect(unsupportedUpdateHandler.handle).toHaveBeenCalledWith({ "callback_query": {} });
        done();
      });
  });

  it('generates a response using unsupported update handler if body has a shipping query', function(done) {
    request(app)
      .post('/bot')
      .send({ "shipping_query": {} })
      .expect(200)
      .expect({ "handler": "unsupported update handler" })
      .end(function(err, res) {
        if (err) return done(err);
        expect(unsupportedUpdateHandler.handle).toHaveBeenCalledWith({ "shipping_query": {} });
        done();
      });
  });

  it('generates a response using unsupported update handler if body has a pre-checkout query', function(done) {
    request(app)
      .post('/bot')
      .send({ "pre_checkout_query": {} })
      .expect(200)
      .expect({ "handler": "unsupported update handler" })
      .end(function(err, res) {
        if (err) return done(err);
        expect(unsupportedUpdateHandler.handle).toHaveBeenCalledWith({ "pre_checkout_query": {} });
        done();
      });
  });

  afterEach(() => {
    expect.restoreSpies();
  });
});
